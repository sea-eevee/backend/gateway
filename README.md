# Eevee Marketplace

## What it is?
Eevee is a marketplace that connects a customer with merchant. To make it simple, the order transaction can only be accepted by an internal employee that acts as an admin. 
## Architecture

Eevee is implemented using microservice architecture. All frontend request will be intercepted by built-in gateway that acts as reverse proxy. The gateway read and proceed the request to corresponding service based on API.

![](img/arch.png)

## Screenshoot
![](img/shoot-1.png)
![](img/shoot-2.png)
![](img/shoot-3.png)
![](img/shoot-4.png)

## How To Install in Local

### 1. Download & Import ke postman

[Marketplace - SEA Gateway](https://documenter.getpostman.com/view/6684309/TVCmQQD6)

Change variable {{server}} on postman with "http://3.89.42.7"

Test ping [http://3.89.42.7/api/ping](http://3.89.42.7/api/ping)

### 2. git clone

```
git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/sso

git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/oms  

git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/customer

git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/admin

git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/merchant

git clone [https://gitlab.com/sea-eevee/backend](https://gitlab.com/sea-eevee/backend)/gateway
```

### 3. Make project folder

backend

- sso
- oms
- customer
- admin
- merchant
- gateway

### 4. Start DB (In separated Terminal to see the logs)

### a. option 1 with make

```bash
$ docker network create --driver bridge compfest-marketplace

$ cd sso
$ make db-reload
$ cd ..

$ cd oms
$ make db-reload
$ cd ..

$ cd merchant
$ make db-reload
$ cd ..

$ cd customer
$ make db-reload
$ cd ..
```

### b. option 2: without make

```bash
$ docker network create --driver bridge compfest-marketplace

$ cd customer
$ docker build -t compfest-marketplace-service-customer-db-img -f Dockerfile.postgres .
$ docker run --rm --name compfest-marketplace-service-customer-db \
        --network=compfest-marketplace \
        -p 13300:5432 \
        -v compfest-marketplace-customer-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=customer \
        compfest-marketplace-service-customer-db-img
$ cd ..

$ cd merchant
$ docker build -t compfest-marketplace-service-merchant-db-img -f Dockerfile.postgres .
$ docker run --rm --name compfest-marketplace-service-merchant-db \
        --network=compfest-marketplace \
        -p 13200:5432 \
        -v compfest-marketplace-merchant-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=merchant \
        compfest-marketplace-service-merchant-db-img
$ cd ..

$ cd ..

$ cd oms
$	docker build -t compfest-marketplace-service-oms-db-img .
$ docker run --rm --name compfest-marketplace-service-oms-db \
        --network=compfest-marketplace \
        -p 13400:5432 \
        -v compfest-marketplace-oms-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=oms \
        compfest-marketplace-service-oms-db-img
$ cd ..

$ cd sso
$ docker build -t compfest-marketplace-service-sso-db-img -f Dockerfile.postgres .
$ docker run -d --rm --name compfest-marketplace-service-sso-db \
        --network=compfest-marketplace \
        -p 14000:5432 \
        -v compfest-marketplace-sso-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=sso \
        compfest-marketplace-service-sso-db-img
```

### 5. Start app (for testing purpose, only run what needed)

```bash
cd admin
go build -o admin
./admin
cd ..

cd merchant
go build -o merchant
./merchant
cd ..

cd customer
go build -o customer
./customer
cd ..

cd sso
go build -o sso
./sso
cd ..

cd gateway
go build -o gateway
./gateway
cd ..

```
