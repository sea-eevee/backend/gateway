package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/gateway/tokenizer"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func NewRouter(tokenizer tokenizer.Contract) *mux.Router {
	root := mux.NewRouter()

	api := root.PathPrefix("/api").Subrouter()

	// health
	api.Methods(http.MethodGet).Path("/ping").HandlerFunc(ping)

	// SSO -- Single Sign On Server
	svcSSO, err := url.Parse("http://compfest-marketplace-service-sso:9000")
	if err != nil {
		panic(err)
	}
	api.PathPrefix("/sso").Handler(http.StripPrefix("/api/sso", httputil.NewSingleHostReverseProxy(svcSSO)))

	// AUTHENTICATED ROUTE
	authed := api.PathPrefix("/authed").Subrouter()
	//authed.Use(tokenizer.MiddlewareAuth)
	{
		// Admin Server
		svcAdmin, err := url.Parse("http://compfest-marketplace-service-admin:8100")
		if err != nil {
			panic(err)
		}
		authed.PathPrefix("/admin").Handler(http.StripPrefix("/api/authed/admin", httputil.NewSingleHostReverseProxy(svcAdmin)))

		// Merchant Server
		svcMerchant, err := url.Parse("http://compfest-marketplace-service-merchant:8200")
		if err != nil {
			panic(err)
		}
		authed.PathPrefix("/merchant").Handler(http.StripPrefix("/api/authed/merchant", httputil.NewSingleHostReverseProxy(svcMerchant)))

		// Customer Server
		svcCustomer, err := url.Parse("http://compfest-marketplace-service-customer:8300")
		if err != nil {
			panic(err)
		}
		authed.PathPrefix("/customer").Handler(http.StripPrefix("/api/authed/customer", httputil.NewSingleHostReverseProxy(svcCustomer)))
	}

	return api
}

func ping(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintf(w, "pong")
}
