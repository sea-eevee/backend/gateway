package tokenizer

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/sea-eevee/backend/gateway/pkg/errs"
	"gitlab.com/sea-eevee/backend/gateway/pkg/responder"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func extractToken(bearToken string) string {
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func (t *tokenizer) MiddlewareAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		clientToken := extractToken(r.Header.Get("Authorization"))
		accessDetail, err := t.decodeTokenDetails(clientToken)
		if err != nil {
			log.Println(err)
			responder.ResponseError(w, err)
			return
		}

		r.Header.Set(KeyHeaderRole, accessDetail.Role)
		r.Header.Set(KeyHeaderUserID, strconv.FormatUint(accessDetail.UserId, 10))
		next.ServeHTTP(w, r)
	})
}

func (t *tokenizer) decodeTokenDetails(rawToken string) (*AccessDetails, error) {
	log.Println(t)
	log.Println(rawToken)
	tokenParsed, err := jwt.Parse(rawToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("secret"), nil
	})
	if err != nil {
		log.Println("err not conform HMAC")
		return nil, err
	}
	claims, ok := tokenParsed.Claims.(jwt.MapClaims)
	if !ok || !tokenParsed.Valid {
		log.Println("err token parsed not valid")
		return nil, err
	}
	role, ok := claims["role"].(string) //convert the interface to string
	if !ok {
		log.Println("err cannot convert role to string")
		return nil, errs.ErrAuth
	}
	userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
	if err != nil {
		log.Println("err cannot convert user_id to uint64")
		return nil, errs.ErrAuth
	}
	return &AccessDetails{
		UserId: userId,
		Role:   role,
	}, nil
}
