package tokenizer

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

func (t *tokenizer) CreateToken(role string, userid uint64) (string, error) {

	atClaims := jwt.MapClaims{
		"role":       role,
		"user_id":    userid,
		"exp":        time.Now().Add(time.Minute * time.Duration(t.atExpires)).Unix(),
		"authorized": true,
	}
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	accessToken, err := at.SignedString("secret")
	if err != nil {
		return "", err
	}

	return accessToken, nil
}
