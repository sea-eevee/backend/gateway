package tokenizer

import (
	"net/http"
)

type Contract interface {
	CreateToken(role string, userid uint64) (string, error)
	MiddlewareAuth(next http.Handler) http.Handler
}

type tokenizer struct {
	secret    string
	atExpires int64
}

func NewTokenizer(secret string, atExpiresMinute int64) Contract {
	return &tokenizer{
		secret:    secret,
		atExpires: atExpiresMinute,
	}
}
