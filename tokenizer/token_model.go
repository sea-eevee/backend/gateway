package tokenizer

type TokenDetails struct {
	AccessToken  string
	RefreshToken string
	AccessUuid   string
	RefreshUuid  string
	Expires      int64
	RtExpires    int64
}

type AccessDetails struct {
	AccessUuid string
	Role       string
	UserId     uint64
}

const (
	KeyHeaderRole   = "X-Role"
	KeyHeaderUserID = "X-UserID"
)
