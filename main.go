package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/gateway/tokenizer"
	"net/http"

	"github.com/gorilla/handlers"
)

func main() {
	tz := tokenizer.NewTokenizer("secret", 200)
	r := NewRouter(tz)
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, err1 := route.GetPathTemplate()
		met, err2 := route.GetMethods()
		fmt.Println(tpl, err1, met, err2)
		return nil
	})

	headersOk := handlers.AllowedHeaders([]string{
		"X-Requested-With",
		"X-UserID",
		"Content-Type",
		"Authorization",
		"Accept",
	})

	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})

	http.ListenAndServe(":80", handlers.CORS(originsOk, headersOk, methodsOk)(r))
}
