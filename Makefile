.PHONY : build run-docker clean

network-create:
	docker network create compfest-marketplace

build:
	docker build -t compfest-marketplace-gateway .

run:
	docker run -d --rm --name compfest-marketplace-gateway \
	--network=compfest-marketplace \
	-p 80:80 \
	compfest-marketplace-gateway

stop:
	docker stop compfest-marketplace-gateway || true

app-reload:
	make stop
	make build
	make run

clean:
	docker rmi -f compfest-marketplace-gateway